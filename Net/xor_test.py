import net as nt
import evolve_net as ev
import numpy as np
import configFile



XOR_X = [[0, 0], [0, 1], [1, 0], [1, 1]]  # Features
XOR_Y = np.array([0, 1, 1, 0])  # Class labels

def evol_loop(genomes):
    num_gen = configFile.num_gen
    genomes = xor_fitness(XOR_X, XOR_Y, genomes)
    for i in range(num_gen):
        #print('generation: ', i+1)
        species = ev.make_species(genomes)
        #print('number of species: ', len(species))
        genomes = ev.pick_parents_make_nets(species)
        genomes = xor_fitness(XOR_X, XOR_Y, genomes)  #fitness update

    species = ev.make_species(genomes)
    return genomes, species


def xor_fitness(XOR_X, XOR_Y, nets):
    for net in nets:
        y = []
        out = []
        for item in XOR_X:
            net.set_in_nodes(item)
            nt.activate_net(net)
            net.set_out_nodes()
            #print('nodes: ', net.nodes)
            for output in net.out_nodes:
                if output > 0.5:
                    y.append(1)
                    out.append(output)
                else:
                    y.append(0)
                    out.append(output)
        #print('ypsilon: ', y)

        net.fitness = 0
        for i, item in enumerate(y):
            if item == XOR_Y[i]:
                net.fitness += 1
        #print('fitness: ', net.fitness)

        #net.fitness -= len(net.hidden_nodes)/20
    return nets


########################XOR TEST###############################

nets = ev.first_generation(XOR_X[0])
evolved_nets, species = evol_loop(nets)

print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~summary all nets~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
print('species: ', len(species))
for net in evolved_nets:
    y= []
    out = []
    for item in XOR_X:
        net.set_in_nodes(item)
        nt.activate_net(net)
        net.set_out_nodes()
        for output in net.out_nodes:
            if output > 0.5:
                y.append(1)
                out.append(output)
            else:
                y.append(0)
                out.append(output)
    #print('genome: ', net.genome)
    #print('nodes: ', net.nodes)
    print('y: ', y)
    print('target: ', [0, 1, 1, 0])
    print('outputs: ', out)
    print('fitness: ', net.fitness)
print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~summary species~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
distance = nt.get_distance(species[0][0],species[1][0])
print('distance between first two species: ',distance)
for nets in species:
    y=[]
    out = []
    for item in XOR_X:
        nets[0].set_in_nodes(item)
        nt.activate_net(nets[0])
        nets[0].set_out_nodes()
        for output in nets[0].out_nodes:
            if output > 0.5:
                y.append(1)
                out.append(output)
            else:
                y.append(0)
                out.append(output)
    #print('genome: ', nets[0].genome)
    #print('nodes: ', nets[0].nodes)
    print('y: ', y)
    print('outputs: ', out)
    print('fitness: ', nets[0].fitness)