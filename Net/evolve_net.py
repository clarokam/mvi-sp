import net as nt
import configFile
import random
import numpy as np
import copy

best_species = []

def first_generation(in_nodes):
    pop_size = configFile.pop_size
    nets = []
    for i in range(pop_size):
        net = nt.Net(in_nodes, True)
        nt.mutate(net)
        nets.append(net)
    return nets

def make_species(nets):
    s = sorted(nets, key=lambda net: net.fitness)
    s = s[::-1]
    species = []
    remainer = s
    a = 1
    while len(remainer) >= 2:
        a += 1

        spec = []
        s = remainer.copy()
        best = s[0]
        spec.append(best)
        s.remove(best)
        remainer.remove(best)

        for i, item in enumerate(s):
            if nt.get_distance(best, item) < configFile.distance:
                spec.append(item)
                remainer.remove(item)

        species.append(spec)
        counter = i

    return species

def pick_parents_make_nets(species):
    pop_size = configFile.pop_size
    nets = []
    global best_species

    #if best_species != []:
        #print('best spec: ', best_species[0].fitness, species[0][0].fitness)
    if configFile.elitism > 0:
        if best_species == []:
            best_species.append(copy.deepcopy(species[0][0]))
        if best_species[0].fitness < species[0][0].fitness:
            best_species = []
            best_species.append(copy.deepcopy(species[0][0]))
            for i in range(configFile.elitism):
                nets.append(species[0][0])
        else:
            for i in range(configFile.elitism):
                nets.append(copy.deepcopy(best_species[0]))

    while pop_size >= len(nets):
        for i,net_array in enumerate(species):
            if i <= int(len(species)/2):                    #first half - better fitness
                if len(net_array) >=2:
                    offspring = nt.crossover(net_array[0], net_array[1])
                    nt.mutate(offspring)
                    nets.append(offspring)
                else:
                    offspring = copy.deepcopy(net_array[0]) # !!!
                    nt.mutate(offspring)
                    nets.append(offspring)

    return nets















