# mutate probabilities
nod_chance_added = 0.1
mutate_weight = 0.5
nod_chance_deleted = 0.1
disable_con = 0.1
add_con = 0.1

# crossover
cross_prob = 0.4

# network
num_in_nodes = 2
num_out_nodes = 1

# population
pop_size = 10
num_gen = 300
fitness = -1000
num_parents = 2

# genetic stuff
distance = 3
elitism = 1





