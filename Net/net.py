import numpy as np
import configFile
import math
import matplotlib.pyplot as plt
import copy
from scipy.special import expit

innovation = []
class Net():

    def __init__(self, in_nodes, id):
        global innovation
        self.fitness = -1
        self.id = id
        self.in_nodes = in_nodes
        self.len_out =  configFile.num_out_nodes
        self.len_in = len(self.in_nodes) + 1 # + 1 is bias
        self.nodes = {}
        self.out_nodes = []

        self.genome =  make_genome(self.len_in, self.len_out)

        # for disabling fully connected net

        # nodes are stored in dictionary
        for i, item in enumerate(self.in_nodes):
            self.nodes[i] = item
        bias = i + 1
        self.nodes[bias] = 1
        for j in range(self.len_out):
            self.nodes[bias + j + 1] = 3

    def set_fitness(self, fitness):
        self.fitness = fitness

    def set_out_nodes(self):
        self.out_nodes = []

        for i in range(self.len_in,(self.len_in+self.len_out)):
            self.out_nodes.append(self.nodes[i])
            return self.out_nodes

    def set_in_nodes(self, in_nodes):
        self.in_nodes = in_nodes
        for i, item in enumerate(self.in_nodes):
            self.nodes[i] = item


def make_genome(len_in, len_out): # all connection
    global innovation
    genome = []
    counter = 0
    for i in range(len_in):
        for j in range(len_in, len_in + len_out): # bias included in len_in
            counter += 1
            innovation.append([counter,[i,j]])
            genome.append([[counter,[i,j]], True])
    for gene in genome:
        make_weight(gene)
    dis = np.random.choice(range(len(genome)-1),np.random.randint((len(genome)-1)))
    #for item in dis:
    #   disable_gene(genome[item])
    for gene in genome:
        gene.append(0)
    return genome

def make_weight(gene):
    mu, sigma = 0, 0.35
    gene.append(np.random.normal(mu, sigma))

# for disabling fully connected net at the very begining
def partialy_disabled(genome):
    picked = np.random.randint(len(genome) - 1)
    disable_gene(genome[picked])


##########################################
####adding nods, connections, mutations
##########################################

def add_nod(net):   #allways new, its made between current connection, old connection is disabled
    global innovation
    new_nod_num = len(net.nodes)
    net.nodes[new_nod_num] = 0
    pick_con = np.random.randint(len(net.genome) - 1)
    innovation.append([len(net.genome)+1, [net.genome[pick_con][0][1][0], new_nod_num]])
    net.genome.append([[len(net.genome)+1,[net.genome[pick_con][0][1][0], new_nod_num]], True, np.random.uniform(-1, 1), 0])
    innovation.append([len(net.genome)+1, [new_nod_num, net.genome[pick_con][0][1][1]]])
    net.genome.append([[len(net.genome)+1, [new_nod_num, net.genome[pick_con][0][1][1]]], True, np.random.uniform(-1, 1), 0])
    net.genome[pick_con][1] = False

def add_con(net): # can become recurrent
    global innovation
    pick_nod1 = np.random.randint(net.len_in, len(net.nodes)) # no new con from input nodes to input nodes
    pick_nod2 = np.random.randint(net.len_in, len(net.nodes))
    for i in range(len(innovation)):
        if [pick_nod1, pick_nod2] not in innovation[i]:
            not_there = True
        else:
            not_there = False
    if not_there:
        innovation.append([len(net.genome) + 1, [pick_nod1, pick_nod2]])
        net.genome.append([[len(net.genome) + 1, [pick_nod1, pick_nod2]], True, np.random.uniform(-1, 1), 0])

def enable_connection(gene):
    gene[1] = True

def disable_gene(gene):
    gene[1] = False

def mutate(net):
    mutate_weight_p = configFile.mutate_weight
    add_nod_p = configFile.nod_chance_added
    disable_con_p = configFile.disable_con
    enable_con_p = configFile.nod_chance_deleted
    add_con_p = configFile.add_con

    for i, item in enumerate(net.genome):
        r = np.random.random()
        if r < mutate_weight_p:  # pro zacatek sance pro vsechny vahy, ze se vyrobi mini cislo, ktere se pricte
            #net.genome[i][2] = np.random.uniform(-1, 1)
            mu, sigma = net.genome[i][2], 0.03
            net.genome[i][2] = np.random.normal(mu, sigma)
        if r < disable_con_p:
            net.genome[i][1] = False
        if r < enable_con_p:
            net.genome[i][1] = True

    r = np.random.random()
    if r < add_nod_p:
        add_nod(net)
    if r < add_con_p:
        add_con(net)


##########################################
####cross over
##########################################

def crossover(net1, net2):
    cross_prob = configFile.cross_prob
    if net1.fitness < net2.fitness: # just for picking longer genome
        netT = net1
        net1 = net2
        net2 = netT

    net3 = copy.deepcopy(net1)
    for i, item in enumerate(net3.genome):
        if i < len(net2.genome):
            p = np.random.random()
            if p < cross_prob and item[1] == net2.genome[i][1]:         #checking for switched off genes
                net3.genome[i] = net2.genome[i]

    return net3

##########################################
####genetic distance
##########################################

def get_distance(net1, net2):
    distance = math.fabs(len(net1.genome)-len(net2.genome))*1.5                      #bigger weight for diference in number of genes
    if len(net1.genome) < len(net2.genome): # just for picking longer genome
        netT = net1
        net1 = net2
        net2 = netT                         # shorter genome

    for i, item in enumerate(net2.genome):
        if item[0][0] == net1.genome[i][0][0] and item[1] == net1.genome[i][1]:
            distance+= (math.fabs(item[2] - net1.genome[i][2]))/2                   #smaller weight for diference in weights

        if item[1] != net1.genome[i][1]:                                            #for switched off genes
            distance += 1/2


    return distance

##########################################
####activation
##########################################
def activate_net(net):
    nodes = net.nodes
    for i in range(net.len_in,len(net.nodes)):
        nodes[i] = 0
    con = []
    for gene in net.genome:
        temp = gene[0][1]
        con.append([temp, gene[2]])      # weights
    touched_nodes = []
    went_through = con
    item = con[0]
    went_through.remove(item)
    while True:
        if item != []:
            nodes[item[0][1]] += math.tan(item[1]*nodes[item[0][0]])
            touched_nodes.append(item)
            a = touched_nodes.count(item)
            if a == 3:
                con.remove(item)
            index = find_next(con,item[0][1])
            if index != -1:
                item = con[index]
                went_through.remove(item)
            elif index == -1 and went_through != []:
                item = con[0]
                went_through.remove(item)

            else:
                for i in range(net.len_in, len(net.nodes)):
                    nodes[i] = sigmoid(nodes[i])
                break
    return nodes

# helper function for activation
def find_next(a, next_nod):
    index = -1 # tohle resi i to, ze con je uz prosly
    for i in range(len(a)):
        if a[i][0][0] == next_nod:
            index = i
    return index

# activation functions
def sigmoid(x):
   return  expit(x)                 # much faster: 1 M loops in 2.9 microsec per loop
  #return 1 / (1 + np.exp(-x))      # 371 ns per loop

##########################################
####for tests
##########################################

def plot_hists(data):
    fig, ax = plt.subplots()
    n_bins = 10
    data_sel = []
    nth = 10
    for i, item in enumerate(data):
        if i % nth == 0:
            data_sel.append(data[i])
    print('delka: ', len(data_sel))
    for i, item in enumerate(data_sel):
        ax.hist(data_sel[i], n_bins, density=True, histtype='bar', stacked=True, alpha=0.5,
                label='gen: ' + str(i * nth))
    ax.legend(prop={'size': 10})
    plt.show()

