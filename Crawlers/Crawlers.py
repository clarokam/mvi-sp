import sys, random
import pygame
import pickle
import neat
from pygame.locals import *
import pymunk
import pymunk.pygame_util
import Creature as cr
import numpy as np
import matplotlib.pyplot as plt
import Button as bt
import Write
# from __future__ import print_function
import neat
import visualize
# just for graph visualisation
#import os
#os.environ["PATH"] += os.pathsep + 'C:/Program Files (x86)/Graphviz2.38/bin/'

# SET THE NUMBER OF GENERATION
gen_num = 50

# global, do not touch
best_last_fitness = -100
best_genome_id = 0
all_means = []
all_data = []
pop_num = 10
shapes = []
muscles = []
bones = []
input = 3
stats = 0



# ground for pygame window
def add_static_ground(space):
    body = pymunk.Body(body_type=pymunk.Body.STATIC)
    body.position = (600, 200)
    ground = pymunk.Segment(body, (-1200, -270), (1200, -270), 100)
    ground.friction = 1.0
    filter = pymunk.ShapeFilter(group=2)
    ground.filter = filter
    ground.collision_type = 0
    ground.elasticity = 0
    space.add(ground)
    return ground

# delete all
def remove_all_objects(space):

    for shape in space.shapes:
        space.remove(shape)

    for constr in space.constraints:
        space.remove(constr)

    for body in space.bodies:
        space.remove(body)

    add_static_ground(space)

# inicialisation of pygame
def create_game():
    pygame.init()
    pygame.font.init()
    font = pygame.font.Font('c:\\Windows\\Fonts\\arial.ttf', 9)
    screen = pygame.display.set_mode((1500, 600))
    pygame.display.set_caption("crawlers")

    return screen

# fitness function for testing genomes
def try_creature(genomes, config):
    global muscles
    global bones
    global shapes
    global stats
    fg = []
    nets = []
    creatures = []
    global best_last_fitness
    global best_genome_id
    generation = len(all_data)

    space = pymunk.Space()
    space.gravity = (0.0, -1500.0)
    add_static_ground(space)
    draw_options = pymunk.pygame_util.DrawOptions(screen)
    draw_options.flags = pymunk.SpaceDebugDrawOptions.DRAW_SHAPES
    clock = pygame.time.Clock()
    text = Write.Writer(100, 0, 'generation: ' + str(generation) + '    best last fitness: ' + str(
        best_last_fitness) + '    id: ' + str(best_genome_id))

    counter = 0

    # CREATION OF GENOMES
    for genome_id, genome in genomes[:]:
        genome.fitness = -1.0

        net = neat.nn.FeedForwardNetwork.create(genome, config)
        nets.append(net)

        creature = cr.temp_to_creature(space, shapes, muscles, bones, genome_id, generation)
        creatures.append(creature)


    # TESTING ALL GENOMES APPLIED ON CREATURES AT ONCE
    sim_len = 1000
    while counter < sim_len:

        event = pygame.event.poll()
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()

        TICK_LENGTH = 20

        for j, creature in enumerate(creatures):

            if counter % TICK_LENGTH == 0:  # prvni tik v kazde davce
                input_vec = []
                velocity = creature.get_velocity(TICK_LENGTH)
                for item in creature.is_touching_ground_vec():
                    input_vec.append(item)
                input_vec.append(creature.get_y_pos())
                input_vec.append(velocity)
                output = nets[j].activate(input_vec)
                #print(input_vec)
                #print(output)

                for i, item in enumerate(output):
                    strength = item
                    creature.set_contract_data(i, strength, TICK_LENGTH)

            # vsechny tiky - jen se pokracuje v tahu
            creature.make_contract()

            if counter == sim_len - 1:
                genomes[j][1].fitness = creature.update_fitness()

        space.step(1 / 50.0)
        screen.fill((255, 255, 255))
        text.write(screen)

        for bd in space.constraints:
            # eventualne if pivotjoint
            pygame.draw.line(screen, (200, 200, 200, 200), (bd.a.position.x, -1 * bd.a.position.y + 600),
                             (bd.b.position.x, -1 * bd.b.position.y + 600), 5)

        space.debug_draw(draw_options)
        pygame.display.flip()

        clock.tick(30)
        counter += 1


        # EXTRACTING FITNESS AND PARTING WITH CREATURES
    for creature in creatures:
        # print('id: ', creature.id, creature.fitness)
        # print(genome_id, genome.fitness)
        fg.append(creature.fitness)
        if creature.fitness > best_last_fitness:
            best_last_fitness = creature.fitness
            best_genome_id = creature.id

        # remove creature
        del creature

    # DATA GATHERING
    print('fg: ', fg)
    all_data.append(fg)
    mean = 0
    for item in fg:
        mean += item
    mean = mean / pop_num
    all_means.append(mean)
    print(all_data)

# some graphs
def make_plot(stats):
    plt.plot(stats)
    print('stats', stats)
    plt.show()

def plot_hists(data):
    fig, ax = plt.subplots()
    n_bins = 3
    data_sel = []
    nth = 10
    for i, item in enumerate(data):
        if i % nth == 0:
            data_sel.append(data[i])
    print('delka: ', len(data_sel))
    for i, item in enumerate(data_sel):
        ax.hist(data_sel[i], n_bins, density=True, histtype='bar', stacked=True, alpha=0.5,
                label='gen: ' + str(i * nth))
    ax.legend(prop={'size': 10})
    plt.show()

# running neat
def run(config):
    global stats
    # population here
    pop = neat.Population(config)

    # reporter
    pop.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    pop.add_reporter(stats)
    pop.add_reporter(neat.Checkpointer(5))

    #pop = neat.Checkpointer.restore_checkpoint('neat-checkpoint-84')
    # creature training - fitness function and number of generation
    win = pop.run(try_creature, gen_num)
    print('Win: '.format(win))

    # results visualisation
    make_plot(all_means)
    #visualize.draw_net(config, win, True, )
    #visualize.plot_stats(stats, ylog=False, view=True)
    #visualize.plot_species(stats, view=True)

def make_config():
        with open('configT', 'r') as f:
            with open('config', 'w') as fout:
                data = []
                for line in f:
                    if line.startswith('num_inputs'):
                        data.append('num_inputs              = ' + str(
                            len(shapes) + 2) + '\n')  # which nod touches the ground, y position, x velocity
                    elif line.startswith('num_outputs'):
                        data.append('num_outputs              = ' + str(len(muscles)) + '\n')
                    else:
                        data.append(line)
                for item in data:
                    fout.write(item)


# STARTING SCREEN WITH TEMPLATE CREATURE CREATION
def start(screen):
    start_button = bt.Button((168, 168, 168), 50, 500, 100, 50, 'start simulation')
    load_button = bt.Button((168, 168, 168), 50, 400, 100, 50, 'load creature')
    save_button = bt.Button((168, 168, 168), 50, 300, 100, 50, 'save creature')
    space = pymunk.Space()
    space.add_collision_handler(1, 1)
    space.gravity = (0.0, 0.0)
    ground = add_static_ground(space)
    draw_options = pymunk.pygame_util.DrawOptions(screen)
    clock = pygame.time.Clock()
    click = True
    LEFT = 1
    MIDDLE = 2
    RIGHT = 3
    selected_s = []
    selected_m = []
    global muscles
    global bones
    global shapes
    muscles = []
    bones = []          # list of indecies of joints with bones connected
    shapes = []         # all points
    musclejoints = []   # points in the middle of bones

    # help box init
    txinfo = ['Controls:', '', 'LMB - add point', 'RMB - select / deselect point or bone', 'MMB - delete point', '', 'B - add bone (with two points selected)', 'M - add muscle (with two bones selected)', '', 'At least one muscle must be defined!']
    font = pygame.font.SysFont('arial', 15)

    text = []
    for line in txinfo:
        text.append(font.render(line, 1, (0, 0, 0)))

    while True:
        event = pygame.event.poll()
        is_rendered = True  # false if screen is just being refreshed when deleting nodes

        # for event in pygame.event.get():
        pos = pygame.mouse.get_pos()
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()

        # delete node
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == MIDDLE:
            pymunk_pos = (pos[0], -pos[1] + 600)

            # nearest shape
            obj = space.point_query_nearest(pymunk_pos, 10.0, pymunk.ShapeFilter())

            if obj != None:
                shape = obj.shape

                # it is a body part
                if shape in shapes:
                    is_rendered = False  # stop checking buttons etc. for one frame
                    nodeindex = shapes.index(shape)
                    shapes.remove(shape)

                    # delete records in all fields
                    pos = 0
                    while pos < len(bones):

                        if bones[pos][0] == nodeindex or bones[pos][1] == nodeindex:

                            # delete muscles
                            musclenumber = 0
                            while musclenumber < len(muscles):
                                if muscles[musclenumber][0] == pos or muscles[musclenumber][1] == pos:
                                    muscles.remove(muscles[musclenumber])
                                    continue

                                musclenumber += 1

                            # renumber all muscles pointing to now non-existing bones
                            for muscle in muscles:
                                if muscle[0] > pos:
                                    muscle[0] -= 1
                                if muscle[1] > pos:
                                    muscle[1] -= 1

                            bones.remove(bones[pos])
                            space.remove(musclejoints[pos])
                            musclejoints.remove(musclejoints[pos])

                            continue

                        else:
                            pos += 1

                    # renumber all bones pointing to now not existing nodes
                    for bone in bones:
                        if bone[0] > nodeindex:
                            bone[0] -= 1
                        if bone[1] > nodeindex:
                            bone[1] -= 1

                    # reload screen
                    remove_all_objects(space)

                    temp = cr.temp_to_creature_temp(space, shapes, muscles, bones, 0, 0)
                    shapes = temp.nods

                    for shape in shapes:
                        shape.color = (0, 0, 220, 255)

                    musclejoints = temp.muscle_nods
                    muscles = temp.muscles_con
                    bones = temp.bones_con



        # MAKE NOD, RIGHT MOUSE
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == RIGHT:
            pymunk_pos = (pos[0], -pos[1] + 600)
            shape = space.point_query_nearest(pymunk_pos, pymunk.inf, pymunk.ShapeFilter()).shape

            if shape != None and len(selected_s) < 2 and shape.radius == 10:
                selected_s.append(shape)
                shape.color = (0, 200, 0, 255)
                for item in selected_m:
                    item.color = (0, 0, 220, 255)
                selected_m = []
            elif shape != None and len(selected_m) < 2 and shape.radius == 8:
                selected_m.append(shape)
                shape.color = (200, 50, 0, 255)
                for item in selected_s:
                    item.color = (0, 0, 220, 255)
                selected_s = []
            else:
                for item in selected_s:
                    item.color = (0, 0, 220, 255)
                selected_s = []
                for item in selected_m:
                    item.color = (0, 0, 220, 255)
                selected_m = []

        if event.type == pygame.MOUSEBUTTONUP and event.button == RIGHT:
            # takhle to nejde, to se musi cele prestavet
            '''
            pos = (pos[0], -pos[1] + 600)
            shape.body.position = pos
            '''

        # BONES
        if event.type == pygame.KEYDOWN and event.key == pygame.K_b:

            if len(selected_s) == 2:
                musclejoints.append(cr.add_bone(space, selected_s[0].body, selected_s[1].body))
                bones.append([shapes.index(selected_s[0]), shapes.index(selected_s[1])])
                for item in selected_s:
                    item.color = (0, 0, 220, 255)

                selected_s = []

        # INFO BUTTON
        if event.type == pygame.KEYDOWN and event.key == pygame.K_c:
            print(bones)

        # MUSCLES
        if event.type == pygame.KEYDOWN and event.key == pygame.K_m:
            if len(selected_m) == 2:
                cr.add_joint(space, selected_m[0].body, selected_m[1].body)
                muscles.append([musclejoints.index(selected_m[0]), musclejoints.index(selected_m[1])])
                for item in selected_m:
                    item.color = (0, 0, 220, 255)
                selected_m = []

        # CREATE NOD
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == LEFT:
            if not start_button.isOver(pos) and not load_button.isOver(pos) and not save_button.isOver(pos):
                if click:
                    position = (pos[0], -pos[1] + 600)  # posun souradnic pymunk vs pygame
                    shapes.append(cr.add_point(space, position, 10, 1))
                    click = False

        # RUN SIMULATION, DEFINE CREATURE OBJECT AND CONFIG FILE
        if event.type == pygame.MOUSEBUTTONUP:
            click = True
            if start_button.isOver(pos):
                if len(muscles)>0:
                    make_config()
                    simulation()

            # SAVING CREATURE TEMPLATE
            elif save_button.isOver(pos):
                fname = 'creatureTemplate'
                fout = open(fname, 'wb')
                pickled_list = [shapes, muscles, bones]
                pickle.dump(pickled_list, fout)
                fout.close()

            # LOADING CREATURE TEMPLATE
            elif load_button.isOver(pos):
                remove_all_objects(space)
                fname = 'creatureTemplate'
                fin = open(fname, 'rb')
                shapes, muscles, bones = pickle.load(fin)
                fin.close()
                temp = cr.temp_to_creature_temp(space, shapes, muscles, bones, 0, 0)
                shapes = temp.nods
                musclejoints = temp.muscle_nods
                muscles = temp.muscles_con
                bones = temp.bones_con
                print(shapes)

        space.step(1 / 50.0)
        screen.fill((255, 255, 255))

        #66 144 244
        r1 = r2 = r3 = 0
        g1 = g2 = g3 = 0
        b1 = b2 = b3 = 0
        rHigh = 66
        gHigh = 144
        bHight = 244

        if is_rendered:

            if start_button.isOver(pos):
                if len(muscles) == 0:
                    r1 = 244
                    g1 = 66
                    b1 = 66

                else:
                    r1 = rHigh
                    g1 = gHigh
                    b1 = bHight

            elif save_button.isOver(pos):
                r2 = rHigh
                g2 = gHigh
                b2 = bHight

            elif load_button.isOver(pos):
                r3 = rHigh
                g3 = gHigh
                b3 = bHight

        start_button.draw(screen, (r1, g1, b1))
        save_button.draw(screen, (r2, g2, b2))
        load_button.draw(screen, (r3, g3, b3))

        # draw lines on bones and muscles
        clr = (200, 200, 200, 200)

        for bd in space.constraints:
            # eventually: if pivotjoint
            pygame.draw.line(screen, clr, (bd.a.position.x, -1 * bd.a.position.y + 600),
                             (bd.b.position.x, -1 * bd.b.position.y + 600), 10)

        # info box
        infostart_x = 1200
        infostart_y = 50
        txstart_x = 10
        txstart_y = 5
        txdistance = 15

        pygame.draw.rect(screen, (0, 0, 0), (infostart_x, infostart_y, 250, 180), 2)

        for line in range(len(text)):
            screen.blit(text[line], (infostart_x + txstart_x, infostart_y+ txstart_y + txdistance*line))

        # pygame.display.update()
        space.debug_draw(draw_options)
        pygame.display.flip()
        clock.tick(30)

def simulation():
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction, neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         'config')
    run(config)

if __name__ == '__main__':
    screen = create_game()
    start(screen)

