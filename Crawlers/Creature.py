import sys, random
import numpy as np
import pygame
from pygame.locals import *
import pymunk
import pymunk.pygame_util
from pygame.color import THECOLORS
import math
from pymunk.vec2d import Vec2d

counter = 0

# basic functions
def add_point(space, position, radius, layer):
    body = pymunk.Body(0.001,1) # weight matters
    body.position = position
    s = pymunk.Circle(body, radius)
    filter = pymunk.ShapeFilter(group=layer)
    s.filter = filter
    s.collision_type = 0
    s.friction = 1.0
    s.elasticity = 0.0
    space.add(s, body)

    return s

def add_bone(space, a_body, b_body):
    body = pymunk.Body(0.05, 100)
    body = pymunk.Body(0.01, 10)
    a_x=a_body.position.x
    a_y=a_body.position.y
    b_x=b_body.position.x
    b_y=b_body.position.y
    position = [(a_x + b_x)/2, (a_y + b_y)/2]
    s = add_point(space, position, 8, 1)
    add_pjoint(space, a_body, s.body,(a_x, a_y))
    add_pjoint(space, s.body, b_body,(b_x, b_y))
    s.color = THECOLORS['pink1']
    #space.add(s, s.body)

    return s

def add_joint(space, body1, body2):
    pivot = pymunk.PinJoint(body1, body2)
    space.add(pivot)
    return pivot

def add_slide_joint(space, body1, body2):
    pivot = pymunk.constraint.SlideJoint(body1, body2, (0,0), (0.0,0.0), 10, 10)
    space.add(pivot)
    return pivot

def add_pjoint(space, body1, body2, pivot):
    pivot = pymunk.PivotJoint(body1, body2, pivot)
    space.add(pivot)
    return pivot

def add_motor(space, body1, body2, rate):
    motor = pymunk.SimpleMotor(body1, body2, 1)
    space.add(motor)
    return motor

def add_ratchet(space, body1, body2, phase, ratched):
    motor = pymunk.RatchetJoint(body1, body2, 1, 45)
    space.add(motor)
    return motor

def add_spring(space, body1, body2):
    a = body1.position
    b = body2.position
    length = math.sqrt((b[0] - a[0]) ** 2 + (b[1] - a[1]) ** 2)
    spring = pymunk.DampedSpring(body1, body2, (0.0, 0.0), (0.0, 0.0), length, 16, 0.2) #Stiffness, dumping
    space.add(spring)
    return spring

# helper function to create creature object from template
def temp_to_creature(space, all_s, muscles_con, bones_con, id, generation):
    origin = Vec2d(120,40)
    nods_pos = []
    bones = []
    dif = 0
    last = 1000
    for i, item in enumerate(all_s):
        if item.body.position.y < last:
            last = item.body.position.y
            dif = item.body.position - origin
    for i, item in enumerate(all_s):
        nods_pos.append(item.body.position - dif)
    cr = Creature(space, nods_pos, muscles_con, bones_con, dif, id, generation)
    return cr

# helper function to create creature object from template
def temp_to_creature_temp(space, all_s, muscles_con, bones_con, id, generation):
    nods_pos = []
    for i, item in enumerate(all_s):
        nods_pos.append(item.body.position )
    cr = Creature(space, nods_pos, muscles_con, bones_con,0, id, generation)
    return cr

def return_color(id):
    if id < len(list(THECOLORS.values())):
        return list(THECOLORS.values())[id]
    else:
        x = id%len(list(THECOLORS.values()))
        return list(THECOLORS.values())[x]

class Creature():

    # nastaveni chovani svalu vsech creatur
    # prvni trojice je distribuce v cisle 0-1 na vstupu, zbyle dve dvojice hladiny kontrakci a extenzi svalu, pokud k nim ma dochazet

    muscle_no_value = 0.2  # 0-1 pravdepodobnost pro zadnou akci (no), stazeni (contract), roztazeni (extent)
    muscle_contract = 0.6 # cislo na vstupu se interpretuje podle pomeru techto tri hodnot
    muscle_extent = 0.2   # priklady: 1 1 1, 0.5 0.25 0.25, 0.5 0.5 0, 1 1 0

    muscle_min_contract = 0.6    # delka svalu kdyz je pri kontrakci nejkratsi (padne nejvyssi cislo z rozsahu)
    muscle_max_contract = 0.8     # delka svalu kdyz je pri kontrakci nejdelsi; pri stejnych hodnotach se vzdy sval stahuje stejne

    muscle_min_extent = 1      # dtto pro roztazeni
    muscle_max_extent = 1.1

    ####################################### spocitane konstanty
    muscle_str_total = muscle_no_value + muscle_contract + muscle_extent
    muscle_str_no = muscle_no_value / muscle_str_total
    muscle_str_contract = muscle_contract / muscle_str_total
    muscle_str_extent = muscle_extent / muscle_str_total



    def __init__(self, space, nods_pos, muscles_con, bones_con, dif, id, generation):
        self.muscles = []
        self.nods = []
        self.nods_bodies = []
        bones = []
        self.muscles_con = muscles_con
        self.bones_con = bones_con
        self.id = id
        self.fitness = 0
        self.muscle_nods = []
        self.original_distance = []
        self.last_x_pos = 0
        self.generation = generation

        #color = random.choice(list(THECOLORS.values()))
        color = return_color(id)
        posun = random.random()# maly posun na zacatku

        self.contract_counter = 0   # aktualni counter - resetuje se na zacatku tiku
        self.contract_counter_base = 0  # originalni hodnota countru kvuli podilu
        self.contract = []   # pro kazdy sval; true stahovani, false roztahovani
        self.muscle_contract_strength = []  # sila kontrakce kazdeho svalu


# NODS
        for pos in nods_pos:
            self.nods.append(add_point(space, pos+posun, 10, 1))
        for item in self.nods:
            item.color = color
            self.nods_bodies.append(item.body)
# BONES
        for item in self.bones_con:
            self.muscle_nods.append(add_bone(space, self.nods_bodies[item[0]], self.nods_bodies[item[1]]))
# MUSCLES
        for item in self.muscles_con:
            self.muscles.append(add_joint(space, self.muscle_nods[item[0]].body, self.muscle_nods[item[1]].body))

        for item in self.muscles:
            self.original_distance.append(item.distance)

# USEFUL FUNCTIONS
    def get_nod_pos_vec(self): # positions of all nodes
        nod_pos_vec = []
        for item in self.nods:
            nod_pos_vec.append(item.body.position[0])
            nod_pos_vec.append(item.body.position[1])
        return nod_pos_vec

    def get_y_pos(self):
        averaged_y_pos = 0
        for item in self.nods:
            averaged_y_pos += (item.body.position[1])
        averaged_y_pos = np.around((averaged_y_pos/len(self.nods))/100, 3)
        return averaged_y_pos

    def get_x_pos(self):
        averaged_x_pos = 0
        for item in self.nods:
            averaged_x_pos += (item.body.position[0])
        averaged_x_pos = np.around((averaged_x_pos/len(self.nods))/1000, 3)
        return averaged_x_pos

    def get_velocity(self, tick_lenght):
        x_pos = 0
        for item in self.nods:
            x_pos += (item.body.position[0])
        x_pos = (x_pos / len(self.nods))
        velocity = np.around(((x_pos - self.last_x_pos) / tick_lenght)/50, 3)
        return velocity

    # ktery nod se dotyka zeme
    def is_touching_ground_vec(self):
        is_touching_ground_vec = []
        for item in self.nods:
            if (item.body.position[1]) < 45:
                is_touching_ground_vec.append(1)
            else:
                is_touching_ground_vec.append(0)
        return is_touching_ground_vec

    def update_fitness(self):
        nod_pos_vec = []
        for item in self.nods:
            nod_pos_vec.append(item.body.position[0])
            nod_pos_vec.append(item.body.position[1])
        self.fitness = nod_pos_vec[0]
        return self.fitness


    # index, inicialni sila, True=kontrakce / jinak roztazeni, kolik tiku se bude stahovat
    def set_contract_data(self, mus_index, start_power, num_ticks):
        self.contract_counter_base = num_ticks
        self.contract_counter = 0
        # spocitat modifikatory

        # nebude se stahovat ani roztahovat
        if start_power <= Creature.muscle_str_no:
            strength = 0

        # bude se stahovat, pak roztahovat
        elif start_power <= Creature.muscle_str_no + Creature.muscle_str_contract:
            # promitnout 0-1 do rozsahu daneho konstantami
            strength = (start_power - Creature.muscle_str_no) / Creature.muscle_str_contract

            # strength je ted cislo 0-1: promitnout do minmax kontrakce
            strength = (Creature.muscle_max_contract-Creature.muscle_min_contract)*strength+Creature.muscle_min_contract

        # bude se roztahovat, pak stahovat; strength bude kladna
        else:
            strength = (start_power - (Creature.muscle_str_no+Creature.muscle_str_contract)) / Creature.muscle_str_extent

            # strength je ted cislo 0-1: promitnout do minmax kontrakce
            strength = (Creature.muscle_max_extent-Creature.muscle_min_extent)*strength+Creature.muscle_min_extent

        if strength != 0:
            strength *= self.original_distance[mus_index]
            strength -= self.original_distance[mus_index]
            strength /= (num_ticks / 2)  # sila jen pro jeden tik

        self.muscle_contract_strength.append(strength)

    # pokracuje v kontrakci vsech svalu
    def make_contract(self):

        for i,item in enumerate(self.muscles):
            if self.muscle_contract_strength[i] != 0:    # ma pracovat

                strength = self.muscle_contract_strength[i]

                if self.contract_counter >= self.contract_counter_base / 2: # v pulce tiku se zmeni znamenko
                    strength = -1 * strength

                item.distance += strength

        self.contract_counter += 1

    def remove_nod(self, nod):
        if nod in self.nods():
            self.nods.remove(nod)
            return True
        return False

    def __del__(self):
        self.joints = []
        self.bodies = []
        self.muscles = []
