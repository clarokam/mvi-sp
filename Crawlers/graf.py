import numpy as np
import matplotlib.pyplot as plt



a = np.zeros((10,), dtype=int)



x = np.random.randn(1000, 3)


def plot_hists(data):
    fig, ax = plt.subplots()
    n_bins = 10
    ax.hist(data, n_bins, density=True, histtype='bar', stacked=True, alpha=0.5, )
    plt.show()

plot_hists(x)