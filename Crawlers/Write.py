import pygame

class Writer():
    def __init__(self,  x, y, text=''):
        self.x = x
        self.y = y

        self.text = text

    def write(self, win):
        if self.text != '':
            font = pygame.font.SysFont('arial', 30)
            text = font.render(self.text, 1, (0, 0, 0))
            win.blit(text, (self.x, self.y))


