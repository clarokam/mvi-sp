# mvi-sp

# NEUROEVOLUTION




## Background
One way how to create functional ANN is to use evolutionary methods generating ANN’s topology and inner parameters. Instead of using supervised learning neuroevolution measure the performance of a given task and use this information for its own evolution akin to algorithms of the nature – survival of the fittest.
 
## Project goal
The goal of this project is to create artificial creatures and train them to move forward. Their ability to move will be used as part of fitness function, that should theoreticaly lead to the evolved neural network capable of creation of suitable body topology and muscle movements.

___________________

# FILES DESCRIPTION

## Crawlers
In this directory you can run script 'Crawlers.py' to open editor with creature's simulation. 
Create creature with nods, bones and muscles to run the simulation, creature needs to have at least one muscle.


## Net
Here can be found network based on evolution algorithms. It can be tried on XOR example by running 'xor_test.py'.

Program was tested on win os, not on Linux.

___________________

Movie with creatures: http://kamclar.cz/sklad/sklad.html

